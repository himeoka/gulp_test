import gulp from 'gulp';
import browserify from 'browserify';
import babelify from 'babelify';
import watchify from 'watchify';
import buffer from 'vinyl-buffer';
import source from 'vinyl-source-stream';
import browserSync from 'browser-sync';
import gulpLoadPlugins from 'gulp-load-plugins';
import imagemin from 'gulp-imagemin';
import php from 'gulp-connect-php';
const $ = gulpLoadPlugins();



function bundle(watching = false) {
  const b = browserify({
    entries: ['src/scripts/main.js'],
    transform: ['babelify'],
    debug: true,
    plugin: (watching) ? [watchify] : null
  })
  .on('update', () => {
    bundler();
  });

  function bundler() {
    return b.bundle()
      .on('error', (err) => {
        console.log(err.message);
      })
      .pipe(source('scripts.js'))
      .pipe(buffer())
      .pipe($.sourcemaps.init({loadMaps: true}))
//      .pipe($.uglify())
//      .pipe($.sourcemaps.write('./'))
      .pipe(gulp.dest('dist/js/'));
  }

  return bundler();
}

// es6のスクリプトをコンパイルしてminify
gulp.task('scripts', () => {
  bundle();
});

// scssをコンパイル
gulp.task('styles', () => {
  gulp.src(`src/styles/**/*.scss`)
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('dist/css'));
});

// cssをminify
gulp.task('cssmin', function () {
    gulp.src('dist/css/styles.css')
        .pipe($.cssmin())
        .pipe($.rename('styles.min.css'))
        .pipe(gulp.dest('dist/css/'));
});
 

// 画像を圧縮
gulp.task('imagemin', () => {
    gulp.src('src/img//**/*.+(jpg|jpeg|png|gif|svg)')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'));
});


// プラグインを結合してminify
gulp.task('plugin-minify', function() {  
var jsFiles = 'src/scripts/plugins/*.js',  
    jsDest = 'dist/js/';
    return gulp.src(jsFiles)
        .pipe($.concat('plugins.js'))
        .pipe(gulp.dest(jsDest))
        .pipe($.rename('plugins.min.js'))
        .pipe($.uglify())
        .pipe(gulp.dest(jsDest));
});


gulp.task('build', ['scripts', 'styles','cssmin']);

gulp.task('watch', () => {
  bundle(true);
  gulp.watch('src/styles/**/*.scss', ['styles']);
  gulp.watch('dist/css/styles.css', ['cssmin']);
});

gulp.task('serve', ['watch'], () => {
  browserSync({
    notify: false,
    port: 9000,
    // proxy: 'example.com',
    server: {
      baseDir: 'dist/'
    }
  });

  gulp.watch([
    'dist/**/*.html',
    'dist/**/*.php',
    'dist/css/**/*.css',
    'dist/js/**/*.js'
  ]).on('change', browserSync.reload);
});

gulp.task('default', ['build', 'serve']);
