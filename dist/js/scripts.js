(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

var _map = require("./map.js");

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  $(function () {

    //vars
    var $contents = $("#contests_wrapp");

    // check template and activate Menu
    function template() {
      var template = $("#contents").attr("data-tmp");
      return template;
    }

    // switch template
    function init() {
      switch (template()) {
        case "index":
          (0, _map2.default)();
          break;
        case "archive":
          break;
        case "single":
          break;
      }
    }

    // 初回ロード時実行
    $contents.addClass("on");
    init();

    // pjaxの設定
    $.pjax({
      area: '#contents', //再描画させるエリア
      link: 'a', // pjaxを発動させるlinkにつけるclass
      ajax: { timeout: 2500 }, // ajaxの通信のタイムアウト時間
      wait: 200, // 押してから発動するまでの時間
      cache: { click: false, submit: false, popstate: false }, // キャッシュの設定
      load: { head: 'base, meta, link', css: false, script: false } // 再描画するエリアを選べる、インラインスクリプトを動作させるかどうか
    });

    // pjax-events

    // データ取得前に発生するイベントを設定できます。
    $(document).on('pjax:fetch', function () {
      $contents.removeClass("on");
    });

    // データの取得後、ページの更新前にwindowオブジェクトから発生します。
    $(window).on('pjax:unload', function () {});

    // areaで指定された範囲のDOMの更新後、documentオブジェクトから発生します。
    $(document).on('pjax:DOMContentLoaded', function () {});

    // すべての更新範囲の描画後、documentオブジェクトから発生します。
    $(document).on('pjax:render', function () {});

    // SCRIPT要素を除くすべてのDOMの更新後、documentオブジェクトから発生します。
    $(document).on('pjax:ready', function () {
      init();
    });

    // すべての画像(IMG要素)とフレーム(IFRAME, FRAME要素)の読み込み後、windowオブジェクトから発生します。
    $(window).on('pjax:load', function () {});

    // すべての更新範囲の描画後、documentオブジェクトから発生します。
    $(document).on('pjax:render', function () {
      $contents.addClass("on");
    });
  });
})(); //import

},{"./map.js":2}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function () {

  // mapを作成
  var canvas = document.getElementById('map');
  var latlng = new google.maps.LatLng(-41.7068439, 171.7236045);
  var bounds = new google.maps.LatLngBounds();
  var mapOptions = {
    center: latlng, // 中心座標 [latlng] 
    mapTypeControlOptions: { mapTypeIds: ['sample', google.maps.MapTypeId.ROADMAP] },
    scrollwheel: false,
    mapTypeControl: false,
    streetViewControl: false
  };

  // デザインをカスタマイズ
  var map = new google.maps.Map(canvas, mapOptions);
  var styleOptions = [{ "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#444444" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#EEEEEE" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": 45 }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#025A7C" }, { "visibility": "on" }] }];

  var styledMapOptions = { name: 'test' };
  var sampleType = new google.maps.StyledMapType(styleOptions, styledMapOptions);
  map.mapTypes.set('sample', sampleType);
  map.setMapTypeId('sample');

  // マーカーのイベント
  var makerAttachEvent = function makerAttachEvent(marker, msg, url) {
    google.maps.event.addListener(marker, 'mouseover', function () {
      msg.open(map, marker);
    });
    google.maps.event.addListener(marker, 'mouseout', function () {
      msg.close();
    });
    google.maps.event.addListener(marker, 'click', function () {
      window.location.href = url;
    });
  };

  var markers = [];

  $.ajax({
    type: 'GET',
    url: '/wp-json/wp/v2/posts',
    dataType: 'json'
  }).done(function (data) {

    // マーカーを作成
    var addMaker = function (data) {
      for (var i = 0; i < data.length; i++) {
        markers.push({
          marker: new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(data[i].acf.map.lat, data[i].acf.map.lng),
            icon: "/img/map/pin.png"
          }),
          infowin: new google.maps.InfoWindow({ content: '<p><img src="' + data[i].acf.photo.sizes.thumbnail + '"></p>' }),
          activity: data[i].pure_taxonomies.categories[0].category_nicename
        });
        bounds.extend(markers[i].marker.position);
        makerAttachEvent(markers[i].marker, markers[i].infowin, data[i].link);
      }
      map.fitBounds(bounds);
    }(data);
  });

  // ソート機能
  var sort = function sort(type, activity, marker) {
    if (type == "all") {
      marker.setVisible(true);
    } else {
      if (activity == type) {
        marker.setVisible(true);
      } else {
        marker.setVisible(false);
      }
    }
  };

  //ソートイベント
  $("#sort_btn").on("click", function () {
    for (var i = 0; i < markers.length; i++) {
      var type = $("#activity_select").val();
      sort(type, markers[i].activity, markers[i].marker);
    }
    $("#sort_outer").removeClass("on");
  });
  $("#sort_display").on("click", function () {
    $("#sort_outer").toggleClass("on");
  });

  //リサイズ
  google.maps.event.addDomListener(window, 'resize', function () {
    map.fitBounds(bounds);
  });
};

},{}]},{},[1])

