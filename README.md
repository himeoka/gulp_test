# README #

Gulpの開発環境テスト

### 環境準備 ###

* リポジトリをクローン後、下記コマンドを実行
```bash
# 必要なgulpのプラグインを下記コマンドでインストール
$ npm install -D gulp browserify babelify babel-preset-es2016 vinyl-source-stream watchify gulp-sourcemaps gulp-uglify vinyl-buffer gulp-load-plugins babel-preset-es2015 browser-sync gulp-plumber gulp-sass gulp-autoprefixer gulp-imagemin gulp-rename gulp-cssmin gulp-concat babel-register gulp-connect-php
```

### 機能 ###

* 開発用のsrcディレクトリからリリース用のdistディレクトリに最適化されたファイルを出力します。
* ソースファイルの変更を監視し、リアルタイムでリリース用ファイルに変更を反映します。
* リリース用ファイルの変更を監視し、変更があった場合はブラウザをリロードし、反映します。
  

es6のコンパイルとminify、sassのコンパイルとminify 
```bash
$gulp build
```
画像の圧縮 
```bash
$gulp imagemin
```
プラグインJSファイルの結合と圧縮
```bash
$gulp plugin-minify
```
ファイルの変更監視と自動
```bash
$gulp watch
```
サーバの立ち上げ、ファイルの変更監視、ファイルが変更されたら自動でブラウザをリロード 
```bash
$gulp serve
```