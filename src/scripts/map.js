export default () => {

// mapを作成
var canvas = document.getElementById( 'map' ) ;
var latlng = new google.maps.LatLng(  -41.7068439 ,171.7236045);
var bounds = new google.maps.LatLngBounds();
var mapOptions = {
center: latlng, // 中心座標 [latlng] 
mapTypeControlOptions: { mapTypeIds: ['sample', google.maps.MapTypeId.ROADMAP] },
scrollwheel: false,
mapTypeControl: false,
streetViewControl: false
}; 


// デザインをカスタマイズ
var map = new google.maps.Map( canvas , mapOptions ) ;
var styleOptions = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#EEEEEE"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#025A7C"},{"visibility":"on"}]}];

var styledMapOptions = { name: 'test' }
var sampleType = new google.maps.StyledMapType(styleOptions, styledMapOptions);
map.mapTypes.set('sample', sampleType);
map.setMapTypeId('sample');


// マーカーのイベント
var makerAttachEvent = function(marker, msg,url) {
google.maps.event.addListener(marker, 'mouseover', function(){
msg.open(map,marker);
});
google.maps.event.addListener(marker, 'mouseout', function(){
msg.close();
});
google.maps.event.addListener(marker, 'click', function(){
window.location.href = url;
});
}


var markers = []

$.ajax({
type: 'GET',
url: '/wp-json/wp/v2/posts',
dataType: 'json'
}).done(function(data){

// マーカーを作成
var addMaker = (function (data) {
for (var i = 0; i < data.length; i++) {
markers.push({
marker:new google.maps.Marker({
map: map,
position: new google.maps.LatLng(data[i].acf.map.lat,data[i].acf.map.lng),
icon:"/img/map/pin.png"
}),
infowin:new google.maps.InfoWindow({content:'<p><img src="'+data[i].acf.photo.sizes.thumbnail+'"></p>'}),
activity:data[i].pure_taxonomies.categories[0].category_nicename
})
bounds.extend (markers[i].marker.position);
makerAttachEvent(markers[i].marker,markers[i].infowin,data[i].link)
}
map.fitBounds (bounds);
})(data);

})

// ソート機能
var sort = function(type,activity,marker){
if(type == "all"){
marker.setVisible(true)  
}else{
if(activity == type){
marker.setVisible(true) 
}else{
marker.setVisible(false) 
}
}
}

//ソートイベント
$("#sort_btn").on("click",function(){
for (var i = 0; i < markers.length; i++) {
var type = $("#activity_select").val();
sort(type,markers[i].activity,markers[i].marker)
}
$("#sort_outer").removeClass("on")
})
$("#sort_display").on("click",function(){
$("#sort_outer").toggleClass("on");
})


//リサイズ
google.maps.event.addDomListener(window, 'resize', function(){
map.fitBounds (bounds);
});


};
