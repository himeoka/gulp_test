//import
import map from './map.js';

(function(){
$(function(){

//vars
var $contents = $("#contests_wrapp");

// check template and activate Menu
function template(){
var template = $("#contents").attr("data-tmp");
return template
}


// switch template
function init(){
switch (template()){
  case "index":
    map();
    break;
  case "archive":
    break;
  case "single":
    break;
}
}



// 初回ロード時実行
$contents.addClass("on")
init();





// pjaxの設定
$.pjax({
area : '#contents', //再描画させるエリア
link : 'a', // pjaxを発動させるlinkにつけるclass
ajax : { timeout: 2500 }, // ajaxの通信のタイムアウト時間
wait : 200, // 押してから発動するまでの時間
cache: { click: false, submit: false, popstate: false },  // キャッシュの設定
load: { head: 'base, meta, link', css: false, script: false }  // 再描画するエリアを選べる、インラインスクリプトを動作させるかどうか
});


// pjax-events

// データ取得前に発生するイベントを設定できます。
$(document).on('pjax:fetch', function(){
	$contents.removeClass("on")
});

// データの取得後、ページの更新前にwindowオブジェクトから発生します。
$(window).on('pjax:unload', function(){
});

// areaで指定された範囲のDOMの更新後、documentオブジェクトから発生します。
$(document).on('pjax:DOMContentLoaded', function(){
});

// すべての更新範囲の描画後、documentオブジェクトから発生します。
$(document).on('pjax:render', function(){
});

// SCRIPT要素を除くすべてのDOMの更新後、documentオブジェクトから発生します。
$(document).on('pjax:ready', function(){
init()
});

// すべての画像(IMG要素)とフレーム(IFRAME, FRAME要素)の読み込み後、windowオブジェクトから発生します。
$(window).on('pjax:load', function(){
});

// すべての更新範囲の描画後、documentオブジェクトから発生します。
$(document).on('pjax:render', function(){
$contents.addClass("on")
});

});

})();






